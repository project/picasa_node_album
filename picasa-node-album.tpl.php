<?php

/**
 * Picasa node album template file
 *
 * $albums is an array containing all image data for the node.
 */
?>
<?php foreach($albums as $key => $album):
if (!is_array($album['images'])):
  continue;
endif; ?>

<div class="album">
  <div class="posted-by"><?php print $album['postedby']; ?></div>

  <?php foreach($album['images'] as $image): ?>
    <a rel="lightbox[picasanodealbum]" href="'. $image['image'] .'"><img src="<?php print $image['thumbnail']; ?>" hspace="2" /></a>
  <?php endforeach; // ends foreach for $album['images'] loop ?>

</div><!-- End album class -->

<?php endforeach; // ends $albums foreach loop ?>