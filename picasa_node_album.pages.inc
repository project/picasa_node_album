<?php

/**
 * @file
 * Page callbacks for the picasa_node_album module
 */

/**
 * Form handler for user/%/edit/picasa
 */
function picasa_node_album_user_edit($form_state, $uid) {

  $form = array();
   
  // accomodate 'me' module
  if (module_exists('me') && $uid == 'me') {
    global $user;
    $uid = $user->uid;
  }

  // load account
  $account = user_load((int)$uid);

  // load google user if one exists
  $google_user = _picasa_node_album_get_user_google_account($account->uid);

  $form['text'] = array(
    '#value' => t('Setting your Google/Picasa username here will allow you to add albums to content.')
  );

  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $account->uid
  );

  $form['google_user'] = _picasa_node_album_get_google_user_field();
  $form['google_user']['#default_value'] = $google_user;

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit')
  );

  //$form['#validate'][] = 'picasa_node_album_validate_handler';

  return $form;
}

/**
 * Implementation of hook_submit().
 */
function picasa_node_album_user_edit_submit($form, $form_state) {

  $result = db_query("SELECT google_user FROM {picasa_node_album_users} WHERE uid = :uid", array(':uid' => $form_state['values']['uid']))->fetchField();
  if ($result) {
    db_query("UPDATE {picasa_node_album_users} SET google_user = :google_user WHERE uid = :uid", array(':google_user' => $form_state['values']['google_user'], ':uid' => $form_state['values']['uid']));
  }
  else {
    db_query("INSERT INTO {picasa_node_album_users} (uid, google_user) VALUES (:uid, :google_user)", array(':uid' => $form_state['values']['uid'], ':google_user' => $form_state['values']['google_user']));
  }
  drupal_set_message(t('Your Picasa settings have been updated.'));
}

/**
 * autoomplete callback for get albums
 */
function picasa_node_album_js_albums($string = '') {
  $albums = _picasa_node_album_get_albums();
  $matches = array();
  if (is_array($albums)) {
    foreach ($albums as $album) {
      if (stripos($album, $string) !== FALSE) {
        $matches[$album] = check_plain($album);
      }
    }
  }
  drupal_json($matches);
}