<?php

/**
 * @file
 * Admin page callbacks for the picasa_node_album module
 */

/**
 * Administration settings for the Zend Framework.
 */
function picasa_node_album_admin() {
  if (!$client = _picasa_node_album_get_client()) {
    drupal_set_message(t('There was a problem connecting to this gmail account. Please update the master account settings.'), 'error');
  }

  $form['master_account'] = array(
    '#type' => 'fieldset',
    '#title' => t('Master Google Account for API Queries')
  );
  $form['master_account']['picasa_node_album_google_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Username'),
    '#description' => t('This is the master account which will be used to login to the Google API'),
    '#size' => 20,
    '#default_value' => variable_get('picasa_node_album_google_user', ''),
  );
  $form['master_account']['picasa_node_album_google_pass'] = array(
    '#type' => 'password',
    '#title' => t('Google Password'),
    '#description' => t('If left blank, it will not override the last password set.'),
    '#size' => 20,
    '#default_value' => '',
  );
  $form['picasa_node_album_accounts'] = array(
    '#type' => 'textarea',
    '#title' => t('Allowed Google Accounts'),
    '#description' => t('A list of Picasa  accounts users are allowed to use. If any account is allowed, leave blank. One account per line.'),
    '#default_value' => variable_get('picasa_node_album_accounts', '')
  );
  $form['picasa_node_album_exclude_accounts'] = array(
    '#type' => 'textarea',
    '#title' => t('Disallowed Google Accounts'),
    '#description' => t('Users will not be allowed to use these accounts. One account name per line.'),
    '#default_value' => variable_get('picasa_node_album_exclude_accounts', '')
  );
  $form['picasa_node_album_default_account'] = array(
    '#type' => 'textfield',
    '#title' => t('Default Picasa Account'),
    '#description' => t('This is the default account that will be active if a user has not selected a custom one.'),
    '#default_value' => variable_get('picasa_node_album_default_account', '')
  );


  $types = array();
  $selected_types = _picasa_node_album_get_node_types();
  $result = db_query("SELECT type, name FROM {node_type} ORDER BY name");
  foreach ($result as $row) {
    $types[$row->type] = $row->name;
  }
  $form['picasa_node_album_node_types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Node Types'),
      '#description' => t('These are the node types you want to enable Picasa Node Album on'),
      '#options' => $types,
      '#default_value' => $selected_types
  );

  $options = array(
    0 => t('Hide Image Captions'),
    1 => t('Show Image Captions'),
    2 => t('Allow users to decide per album')
  );
  $form['picasa_node_album_captions'] = array(
    '#type' => 'radios',
    '#title' => t('Image Caption Settings'),
    '#description' => t('This setting exposes the image title to the templates.'),
    '#options' => $options,
    '#default_value' => variable_get('picasa_node_album_captions', 1)
  );
  $form['picasa_node_album_per_node'] = array(
    '#type' => 'textfield',
    '#title' => t('Albums Per Node'),
    '#description' => t('The number of albums per node users are allowed to upload. Set to 0 for unlimited.'),
    '#default_value' => variable_get('picasa_node_album_per_node', 1),
    '#size' => 8
  );

  $options = array( '0' => t('<none>'),
                    '60' => t('1 min'),
                    '180' => t('3 min'),
                    '300' => t('5 min'),
                    '600' => t('10 min'),
                    '900' => t('15 min'),
                    '1800' => t('30 min'),
                    '2700' => t('45 min'),
                    '3600' => t('1 hour'),
                    '10800' => t('3 hours'),
                    '21600' => t('6 hours'),
                    '32400' => t('9 hours'),
                    '43200' => t('12 hours'),
                    '86400' => t('1 day'),
                    );
  $form['picasa_node_album_cache_min'] = array(
    '#type' => 'select',
    '#title' => t('Minimum cache lifetime'),
    '#description' => t('Your server needs to contact Google every time it loads fresh album data. If you set a cache lifetime of 1 hour, then your server will store the results locally and only contact Google every hour.'),
    '#options' => $options,
    '#default_value' => variable_get('picasa_node_album_cache_min', 21600)
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save Settings'));
  return $form;
}

/**
 * Submit handler. Had to make it custom to handle the password logic
 */
function picasa_node_album_admin_submit(&$form, $form_state) {

  variable_set('picasa_node_album_google_user', $form_state['values']['picasa_node_album_google_user']);
  // only set the pass if they submitted a value
  if ($form_state['values']['picasa_node_album_google_pass']) {
    variable_set('picasa_node_album_google_pass', $form_state['values']['picasa_node_album_google_pass']);
  }
  variable_set('picasa_node_album_accounts', $form_state['values']['picasa_node_album_accounts']);
  variable_set('picasa_node_album_exclude_accounts', $form_state['values']['picasa_node_album_exclude_accounts']);
  variable_set('picasa_node_album_default_account', $form_state['values']['picasa_node_album_default_account']);
  variable_set('picasa_node_album_captions', $form_state['values']['picasa_node_album_captions']);
  variable_set('picasa_node_album_per_node', $form_state['values']['picasa_node_album_per_node']);
  variable_set('picasa_node_album_node_types', serialize($form_state['values']['picasa_node_album_node_types']));
  variable_set('picasa_node_album_cache_min', $form_state['values']['picasa_node_album_cache_min']);

  // now clear menu cache since there is logic
  module_invoke('menu', 'rebuild');

  drupal_set_message(t('Picasa node album settings saved successfully.'));
}

